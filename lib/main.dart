import 'package:basic_physics_based_animation/widgets/draggable_card.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Physics-Based Animation Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home:
          PhysicsBasedAnimationDemo(title: 'Physics-Based Animation Demo Page'),
    );
  }
}

class PhysicsBasedAnimationDemo extends StatefulWidget {
  final String title;

  const PhysicsBasedAnimationDemo({Key key, this.title}) : super(key: key);

  @override
  _PhysicsBasedAnimationDemoState createState() =>
      _PhysicsBasedAnimationDemoState();
}

class _PhysicsBasedAnimationDemoState extends State<PhysicsBasedAnimationDemo> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: DraggableCard(
        child: FlutterLogo(
          size: 128,
        ),
      ),
    );
  }
}
